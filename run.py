from flask import Flask, request, jsonify
from flask_cors import CORS

from show_milhao import db

app = Flask('Show do Milhão')
CORS(app,resources={r"*":{"origins": "*"}})

@app.route('/')
def hello_world():
    return 'Hello, Jogadores!'

def montar_alternativas(id_pergunta, outras_perguntas):
    alternativas = []

    for outra in outras_perguntas:
        if outra[6] == id_pergunta:
            alternativas.append({'id': outra[4], 'descricao': outra[5]}) 

    return alternativas


def ja_colocou(id_pergunta, lista):
    achou = False

    for pergunta in lista:
        if pergunta['id_pergunta'] == id_pergunta:
            achou = True
    
    return achou

@app.route('/perguntas')
def retorna_perguntas():
    perguntas = db.executar_query("select perguntas.id, perguntas.titulo, perguntas.resposta_correta, perguntas.dificuldade, alternativas.id, alternativas.descricao, alternativas.id_pergunta from perguntas, alternativas where perguntas.id = alternativas.id_pergunta")
    
    lista = []

    tabela = ['id_pergunta', 'titulo', 'resposta', 'dificuldade']
   
    for pergunta in perguntas: 
        dicionario = {}

        for i in range(len(tabela)):
            dicionario[tabela[i]] = pergunta[i]
            dicionario['alternativas'] = montar_alternativas(pergunta[0], perguntas)        

        if not ja_colocou(pergunta[0], lista):
            lista.append(dicionario)

    return jsonify(lista)

@app.route('/jogador', methods = ['POST'])
def enviar_pontucao():
    nome_jogador = request.json['nome']
    pontuacao = request.json['pontuacao']

    print(nome_jogador)
    print(pontuacao)

    var = "insert into ranking (nome, premio) values ('" + nome_jogador +"', '" + pontuacao + "')"
    db.executar_query(var)

    return jsonify({'status': 'ok'})


@app.route('/ranking')
def retorna_ranking():
    ranking = db.executar_query('select nome, premio from ranking order by premio desc limit 10')

    lista = []

    for item in ranking:
        dicionario = {
            'nome': item[0],
            'pontuacao': item[1]
        }

        lista.append(dicionario)

    
    return jsonify(lista)