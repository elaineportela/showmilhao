import sqlite3
import os
from flask import g

DATABASE = './show_do_milhao.db'

def executar_query(query, params = tuple()):
    db = sqlite3.connect(DATABASE)
    cursor = db.cursor()
    cursor.execute(query, params)
    db.commit()

    return cursor.fetchall() 